<?php

class Employee extends Person {

	private $ssn; 
	private $gender;  

	public function __construct($fn = "Peter", $ln = "Paulsen", $ag = 33, $s = '123456789', $g = 'male')
	{
		$this->ssn = $s; 
		$this->gender = $g; 

		parent::__construct($fn,$ln,$ag); 

		echo "Creating <b>"; 
		echo person::GetFname();
		echo " ";
		echo person::GetLname();
		echo " is </b>";
		echo person::GetAge();
		echo " with ssn </b>";
		echo $this->ssn; 
		echo " and is </b>";
		echo $this->gender;
		echo "</b> employee object from parameterized constructor (accepts five arguments ): <br>";
	}

	function __destruct()
	{
		parent::__destruct(); 

		echo "Destroying <b>"; 
		echo person::GetFname();
		echo " ";
		echo person::GetLname();
		echo " is </b>";
		echo person::GetAge();
		echo " with ssn </b>";
		echo $this->ssn; 
		echo " and is </b>";
		echo $this->gender;
		echo "</b> employee object <br>";
	} 

	public function SetSSN($s = " 111-11-1111 ")
	{
		$this->ssn = $s; 
	}

	public function SetGender($g = 'f')
	{
		$this->gender = $g; 
	}

	public function GetSSN()
	{
		return $this->ssn; 
	}

	public function GetGender()
	{
		return $this->gender; 
	}
}



?>