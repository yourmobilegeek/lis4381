> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 5 Requirements:

*Two Parts:*

1. Screenshot index page & passed validation 
2. Bitbucket repo link
3. Include server side validation and regular expressions as per the database

#### README.md file should include the following items:

* Screenshot of failed validation;
* Screenshot of passed validation;

#### Assignment Screenshots:

* Screenshot of index validation;

![ERD Screenshot](img/index_validation.png)

* Screenshot of failed validation;

![ERD Screenshot](img/failed_validation.png)
