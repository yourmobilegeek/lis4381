> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis

### Project 2 Requirements:

*Two Parts:*

1. Screenshot index page & passed validation 
2. Bitbucket repo link
3. Include server side validation and regular expressions as per the database

#### README.md file should include the following items:

* Screenshot of index page;
* Screenshot of edit page;
* Screenshot of error page;

#### Assignment Screenshots:

* Screenshot of index page;

![ERD Screenshot](img/index_validation.png)

* Screenshot of edit page;

![ERD Screenshot](img/edit_validation.png)

* Screenshot of error page;

![ERD Screenshot](img/error_validation.png)