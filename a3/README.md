> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 3 Requirements:

*Three Parts:*

1. Database based upon business rules  
2. Screenshot of ERD 
3. Links to A3 mwb & sql 
4. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application

#### Assignment Screenshots:

**1. Screenshot of Ticket Value App:**

![First Screen](img/first.png "Android Dev Screen")

![Second Screen](img/second.png "Android Dev Screen")

**2. Screenshot of ERD:**

![Pet Store ERD Screen](img/a3_erd.png)

**3. MBW File:**

[Pet Store MWB File](docs/a3.mwb "MWB file")

**4.SQL File:**

[Pet Store SQL File](docs/a3.sql "SQL file")
