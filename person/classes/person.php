<?php

class Person {

	private $fname; 
	private $lname; 
	private $age; 

	public function __construct($fn = "John", $ln = "Doe", $ag = 21)
	{
		$this->fname = $fn; 
		$this->lname = $ln; 
		$this->age = $ag; 

		echo "Creating <b>"; 
		echo $this->fname;
		echo " ";
		echo $this->lname;
		echo " who is </b>";
		echo $this->age;
		echo "</b> person object from parameterized constructor (accepts three arguments ): <br>";
	}

	function __destruct()
	{
		echo "Destroying <b>";
		echo $this->fname; 
		echo " "; 
		echo $this->lname; 
		echo " who is </b>"; 
		echo $this->age; 
		echo "</b> person object. <br>";
	} 

	public function SetFname($fn = " John ")
	{
		$this->fname = $fn; 
	}

	public function SetLname($ln = " Doe ")
	{
		$this->lname = $ln; 
	}

	public function SetAge($ag = 21 )
	{
		$this->age = $ag; 
	}

	public function GetFname()
	{
		return $this->fname; 
	}

	public function GetLname()
	{
		return $this->lname; 
	}

	public function GetAge()
	{
		return $this->age; 
	}

}



?>