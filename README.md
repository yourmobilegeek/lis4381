> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
> 

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis 

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Instal AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Ticket Value Andriod app
    - Provide screenshots of completed app & ERD 
    - Create ERD based upon business rules
    - Provide DB resource links
    - Provide screenshots of completed app 

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create index page, with carousel
    - Create working links for other folders
    - Create P1 index page
    - Validate user input to each specific type
    - Give error messages when a user inputs wrong values
    - Show user when input has been properly inputed

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use validation to fully submit user input
    - Create fully functional delete and edit buttons for the database
    - Give error message upon wrong server side validation

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a personal business card App
    - Use native programming for Android for the two-page App
    - Add background color for the App title, and main two pages
    - Change avatar picture
    - Use radio buttons, images , text and launcher icon

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create a server side validation for 'store' database
    - Upload database with proper edits

