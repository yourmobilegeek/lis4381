> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis

### Project 1 Requirements:

*Three Parts:*

1. Course title, your name, assignment requirements, as per A1; 
2. Screenshot of running application’s first user interface;
3. Screenshot of running application’s second user interface;

#### README.md file should include the following items:

* Screenshot of running application

#### Assignment Screenshots:

**1. Screenshot of My Business Card App:**

![First Screen](img/first.png "Android Dev Screen")

![Second Screen](img/second.png "Android Dev Screen")