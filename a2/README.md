> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Ariana M. Davis

### Assignment 2 Requirements:

*Three Parts:*

1. Haelthy Recipe Mobile App 
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of runninf mobile app

#### Assignment Screenshots:

*Screenshot of Healthy Recipe - Mobile App*:

![Home Screen](img/screen1.png)

![Second Screen](img/screen2.png)